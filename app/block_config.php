<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class block_config extends Model
{
  protected $table = 'block_config';
  public $timestamps = false;
  //protected $primaryKey = 'id';
  protected $fillable =[
    'block_date',
    'alloved_month',
    'date_interval',
    'days',
    'hours',
    'minute',
    'app_title',
    'date_format',
    'app_status',
    'datepicker_label'
  ];
  
}
